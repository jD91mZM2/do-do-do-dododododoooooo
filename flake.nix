{
  outputs = { self, nixpkgs }: let
    forAllSystems = nixpkgs.lib.genAttrs [ "x86_64-linux" ];
  in {
    packages = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in {
      do-do-do-dododododoooooo = pkgs.poetry2nix.mkPoetryApplication {
        projectDir = ./.;
      };
    });

    defaultPackage = forAllSystems (system: self.packages."${system}".do-do-do-dododododoooooo);

    devShell = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in pkgs.mkShell {
      nativeBuildInputs = [
        pkgs.poetry

        (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
        })
      ];
    });
  };
}
