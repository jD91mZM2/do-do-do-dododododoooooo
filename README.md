# Do... do... do... dododododoooooooooooo

This is the most useful application you've ever seen. It takes a MIDI input
file and generates an SVG consisting of just the word "do", which I call a
do-song. Really fun to challenge friends with, "guess the song". They can't
play it or anything, they just need to sing it in their heads.

Too advanced midi-files may become an unreadable mess, but if you delete some
tracks it should be fine ¯\\\_(ツ)\_/¯

## Example

Due to copyright laws, I am unable to show you a *cool* midi, but I can show
you a public-domain song I arranged in [MuseScore 3](https://musescore.org/en).

[This midi file](./examples/Head_Shoulders_Knees_and_Toes.mid)
gets converted to ![this
do-song](./examples/Head_Shoulders_Knees_and_Toes.svg).
