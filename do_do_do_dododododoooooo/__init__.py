#!/usr/bin/env python

from argparse import ArgumentParser
from collections import namedtuple
from pathlib import Path

from mido import MidiFile
from svgwrite import Drawing

MAX_WIDTH = 1000
LINE_HEIGHT = 150
TIME_SCALE = 100

Note = namedtuple("Note", "time")


def main():
    parser = ArgumentParser(description="Convert a MIDI to do-songs")
    parser.add_argument("file", type=Path, help="Midi file to convert from")
    parser.add_argument("output", type=Path, help="Output SVG image")
    args = parser.parse_args()

    components = []

    pressed_keys = {}

    current_time = 0
    max_y = 0

    for msg in MidiFile(args.file):
        current_time += msg.time
        if msg.type == "note_on" and msg.velocity > 0:
            print(msg)
            pressed_keys[msg.note] = Note(
                time=current_time
            )
        elif msg.type == "note_on" or msg.type == "note_off":
            key = pressed_keys.pop(msg.note, Note(
                time=current_time
            ))
            relative_note = msg.note - 61

            real_x = key.time * TIME_SCALE
            line = 1.0 + (real_x // MAX_WIDTH)

            x = real_x % MAX_WIDTH
            y = line*LINE_HEIGHT - LINE_HEIGHT/2 - 2*relative_note
            max_y = max(max_y, y)

            duration = current_time - key.time
            text = "d" + "o" * int(duration * 5)

            components.append(("text", {
                "text": text,
                "insert": (f"{x}px", f"{y}px"),
            }))

    max_x = MAX_WIDTH + TIME_SCALE
    max_y += LINE_HEIGHT

    drawing = Drawing(
        args.output,
        profile="tiny",
        size=(f"{max_x}px", f"{max_y}px")
    )

    for y in range(0, int(max_y), LINE_HEIGHT):
        drawing.add(drawing.line((0, y), (max_x, y), stroke="black"))

    for ty, kwargs in components:
        drawing.add(getattr(drawing, ty)(**kwargs))

    drawing.save()
